<?php

include 'core/init.php';

include 'vendor/autoload.php';

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('xolf UG');
$pdf->SetTitle('Fleamarket - Seller List');
$pdf->SetSubject('Seller List');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);

// add a page
$pdf->AddPage();

$html = '<p style="font-size: 13px">
<table>
    <tr style="padding-bottom: 10px">
        <th>Verk&auml;ufer Nummer</th>
        <th>Umsatzerl&ouml;s</th>
        <th>Auszahlungsbetrag</th>
    </tr>';
$result = $mysql->query("SELECT *,sum(`price`) as 'moneyback' FROM `sold_objects` GROUP BY `seller` ORDER BY `seller`");

$i = 1;

while($data = mysqli_fetch_assoc($result))
{

    $moneyback = strToFloat($data['moneyback']) * 0.85;

     $html .= '
        <tr>
            <td style="color: red">V'.$data['seller'].'</td>
            <td style="color: blue">'.seePrice($data['moneyback'], FALSE).'</td>
            <td style="color: blue">'.seePrice($moneyback, FALSE).'</td>
        </tr>';

    $i++;

}
$html .='
</table>
</p>';

$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();

$pdf->Output('flea-market.pdf', 'I');

?>
