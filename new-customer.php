<?php

include 'core/init.php';

include 'template/head.php';

if($_GET['n'] == 'y')
{

    foreach($_SESSION['flea-market']['objects'] as $data)
    {

        $mysql_data = array(
            'id' => NULL,
            'seller' => $data['seller'],
            'price' => $data['price'],
            'device' => $_COOKIE['device-name'],
            'time' => time()
        );

        $mysql->insert($mysql_data, 'sold_objects');

    }

    unset($_SESSION['flea-market']['objects']);

    $query = "UPDATE `devices` SET `objects` = 'null'";

    $mysql->query($query);

    header('Location: add-object.php');

}

?>

    <div class="container">
        <div class="col-md-3">

        </div>
        <div class="col-md-6">
            <h3 class="full-width">N&auml;chster Vorgang mit einem neuen Kunden starten?</h3>
            <a href="checkout.php" class="btn btn-lg btn-default left goLeft">Nein</a>
            <a href="new-customer.php?n=y" class="btn btn-lg btn-primary right goRight">Ja</a>
        </div>
        <div class="col-md-3">

        </div>
    </div>

<?php

include 'template/foot.php';

?>