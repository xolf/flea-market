<?php

include 'core/init.php';

include 'template/head.php';

?>

    <div class="container">
        <h1>Auszahlungsbetrag</h1>
        <a href="pdf.php" class="btn btn-primary">Als PDF Datei</a>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Verk&auml;ufer Nummer</th>
                <th>Umsatzerl&ouml;s</th>
                <th>Auszahlungsbetrag <small>(15% abgezogen)</small></th>
            </tr>
            </thead>
            <tbody>
            <?php

            $result = $mysql->query("SELECT *,sum(`price`) as 'moneyback' FROM `sold_objects` GROUP BY `seller` ORDER BY `seller`");

            $i = 1;

            while($data = mysqli_fetch_assoc($result))
            {

                $moneyback = strToFloat($data['moneyback']) * 0.85;

                echo '
        <tr>
            <td>'.$i.'</td>
            <td class="seller"><a href="seller.php?i='.$data['seller'].'">V'.$data['seller'].'</a></td>
            <td class="price">'.seePrice($data['moneyback'], FALSE).'</td>
            <td class="price">'.seePrice($moneyback, FALSE).'</td>
        </tr>';

                $i++;

            }

            ?>
            </tbody>
        </table>
    </div>

<?php

include 'template/foot.php';
