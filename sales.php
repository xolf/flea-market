<?php

include 'core/init.php';

include 'template/head.php';

?>

<div class="container">
    <h1>Bisherige Verk&auml;ufe</h1>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Verk&auml;ufer Nummer</th>
            <th>Preis</th>
            <th>Ger&auml;t</th>
            <th>Datum</th>
        </tr>
        </thead>
        <tbody>
        <?php

        $result = $mysql->query("SELECT * FROM `sold_objects` ORDER BY `id` DESC");

        while($data = mysqli_fetch_assoc($result))
        {

            echo '
        <tr>
            <td>'.$data['id'].'</td>
            <td class="seller">V'.$data['seller'].'</td>
            <td class="price">'.seePrice($data['price'], FALSE).'</td>
            <td>'.$data['device'].'</td>
            <td>'.date('j M Y - H:i', $data['time']).'</td>
        </tr>';

        }

        ?>
        </tbody>
    </table>
</div>

<?php

include 'template/foot.php';
