<?php

error_reporting(E_ALL ^ E_NOTICE);

session_start();

setlocale(LC_MONETARY, 'de_DE');

include __DIR__ . '/ownExceptionHandler.php';

include __DIR__ . '/mysqlDriver.php';

$mysql = new ownMysql('localhost','root','root','fleamarket');

$mysql->connect();

include __DIR__ . '/function.php';

include __DIR__ . '/device.php';
