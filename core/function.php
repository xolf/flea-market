<?php
function strToFloat($string)
{

    $string = str_replace(',','.',$string);

    $string = floatval($string);

    return $string;

}

function seePrice($data, $echo = TRUE)
{

    $decimal_add = '';

    $decimal = FALSE;

    if(is_string($data))
    {

        $data = strToFloat($data);

    }

    $data = round($data, 2);

    if(str_replace('.','d',$data)) $decimal = explode('.',$data)[1];

    if(strlen($decimal) == 0) $decimal_add = '.00';

    if(strlen($decimal) == 1) $decimal_add = '0';

    $data = str_replace('.',',',$data . $decimal_add . '&euro;');

    if(!$echo) return $data;

    echo $data;

}