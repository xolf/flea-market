<?php

include 'core/init.php';

if(isset($_GET['i']))
{

    $i = $_GET['i'] - 1;

    unset($_SESSION['flea-market']['objects'][$i]);

    $_SESSION['flea-market']['objects'] = array_values($_SESSION['flea-market']['objects']);

    header('Location: cart.php');


}

include 'template/head.php';

include 'template/foot.php';
