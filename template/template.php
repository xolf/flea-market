
    <div class="sell-item">
        <h3>Verk&auml;ufer Nummer</h3>
        <div class="input-group">
            <span class="input-group-addon" id="addon" name="seller">V</span>
            <input id="seller" type="number" class="form-control input-lg" aria-describedby="addon" name="seller">
        </div>
        <div class="numpad" id="numpad-seller">
            <?php include getView('template.numpad') ?>
        </div>
        <h3>Preis</h3>
        <div class="input-group">
            <input id="price" type="text" pattern="[0-9]+([\.|,][0-9]+)?" step="0.01" class="form-control input-lg" aria-describedby="addon" name="price">
            <span class="input-group-addon" id="addon" name="price">€</span>
        </div>
        <div class="numpad" id="numpad-price">
            <?php include getView('template.numpad') ?>
        </div>
        <div class="input-group">
            <input type="submit" value="Eintragen" class="btn btn-primary btn-lg">
        </div>

    </div>
