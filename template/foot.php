
</div>
<div class="reboot" style="display: none">
    <h1 style="display: block; text-align: center; margin: 50px 20px">System Neustart</h1>
    <hr>
    <p style="display: block; text-align: center; margin: 10px">In 2 Minuten wird wieder alles erreichbar sein<br><b>Nicht</b> die Seite neuladen</p>
</div>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/app.js"></script>
<script src="js/charts.js"></script>
<script>
$(document).ready(function(){
    setInterval(function(){
        $.ajax({
            url: "/",
            type: "GET",
	        timeout: 1000,
	        async: true,
	        error: function( xhr, status, errorThrown) {
                console.log( "Error: " + errorThrown );
                console.log( "Status: " + status );
                console.dir( xhr );
                $(".container").css('display', 'none');
                $(".reboot").css('display', 'block');
            },
            success: function ( data ) {
                $(".container").css('display', 'block');
                $(".reboot").css('display', 'none');
            }
        });
    }, 2 * 1000);
});
</script>
<?php if($chart) : ?>
    <script>

        $(function () {
            $(document).ready(function () {
                Highcharts.setOptions({
                    global: {
                        useUTC: false
                    }
                });

                $('#container').highcharts({
                    chart: {
                        type: 'spline',
                        animation: Highcharts.svg, // don't animate in old IE
                        marginRight: 10,
                    },
                    title: {
                        text: 'Live random data'
                    },
                    xAxis: {
                        tickPixelInterval: 150
                    },
                    yAxis: {
                        title: {
                            text: 'Value'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    legend: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    series: [{
                        name: 'Random data',
                        data: [
                            <?php

                            $time = time() - 30;

                            $result = $mysql->query("SELECT count(`id`),`time` AS `count` FROM `sold_objects` GROUP BY `time` DESC LIMIT 15 ");

                            echo 1;

                            while($data = mysqli_fetch_assoc($result))
                            {

                                var_dump($data);

                            }

                            ?>
                        ]
                    }]
                });
            });
        });
    </script>
<?php endif; ?>
</body>
</html>
