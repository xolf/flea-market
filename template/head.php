<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Fleamarket System</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/global.css" rel="stylesheet">
</head>

<body>

<div class="container">

    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Flea market</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="add-object.php">Neuer Artikel</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Verwaltung <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-header">Buchhaltung</li>
                            <li><a href="sales.php">Bisherige Verk&auml;ufe</a></li>
                            <li><a href="money-back.php">Auszahlungsbetrag</a></li>
                            <li><a href="profit.php">Profit</a></li>
                            <li><a href="device-amount.php">Kassenstand</a></li>
                            <li role="separator" class="divider"></li>
                            <li class="dropdown-header">Technick</li>
                            <li><a href="devices.php">Kassenliste</a></li>
                            <li><a href="destroy-device.php">Ger&auml;t abmelden</a></li>
                            <li><a href="poweroff.php">System abschalten</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>