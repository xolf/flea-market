$('#numpad-seller .btn').click(function(){
    var attr = $(this).attr('data-value');
    if(attr == 'del' || attr == 'com'){
        if(attr == 'del'){
            str = $('#seller').val();
            $('#seller').val(str.substring(0, str.length-1));
        }
    }else{
        $('#seller').val($('#seller').val() + attr);
    }
});


$('#numpad-price .btn').click(function(){
    var preCode = '',afterCode = '';
    var attr = $(this).attr('data-value'),
    str = $('#price').val().replace(',','');

    if(attr == 'del' || attr == 'com'){
        if(attr == 'del'){
            $('#price').val(str.substring(0, str.length-1));
        }
        if(attr == 'com'){
            $('#price').val(str + '.');
        }
    }else{
        str = str + attr;
        if(str.substr(0, 1) == 0) str = str.substr(1, str.length);
        console.log(str);
        var afterCode = str.substring(str.length - 2, str.length),
            preCode = str.substr(0, str.length - 2);
        if(afterCode.length == 1) afterCode = '0' + afterCode;
        if(preCode.length == 0) preCode = '0';

        //console.log(preCode + ',' + afterCode);
        $('#price').val(preCode + ',' + afterCode);
    }
});

window.onkeyup = function(e) {
    var key = e.keyCode ? e.keyCode : e.which;

    if (key == 37) {
    //Left
        if($('.goLeft').attr('href') != undefined) window.location = $('.goLeft').attr('href');
    }else if (key == 39) {
    //Right
        if($('.goRight').attr('href') != undefined) window.location = $('.goRight').attr('href');
    }

}
