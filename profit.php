<?php

include 'core/init.php';

include 'template/head.php';

?>

    <div class="container">

        <?php

            $result = $mysql->query("SELECT sum(`price`) * 0.15 as 'profit' FROM `sold_objects` ");

            echo '<h1 class="full-width">Profit von ' . seePrice(mysqli_fetch_assoc($result)['profit'], FALSE) . '</h1>';

            ?>
    </div>

<?php

include 'template/foot.php';
