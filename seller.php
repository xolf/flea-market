<?php

include 'core/init.php';

include 'template/head.php';

?>

    <div class="container">
        <a class="left goLeft btn btn-lg btn-default" href="money-back.php">Zur&uuml;ck <<</a>
        <h1>Verk&auml;ufer <span class="seller">V<?php echo $_GET['i'] ?></span></h1>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Preis</th>
                <th>Ger&auml;t</th>
                <th>Datum</th>
            </tr>
            </thead>
            <tbody>
            <?php

            $result = $mysql->query("SELECT * FROM `sold_objects` WHERE `seller` = '".$_GET['i']."' ORDER BY `id`");

            while($data = mysqli_fetch_assoc($result))
            {

                $price += $data['price'];

                echo '
        <tr>
            <td>'.$data['id'].'</td>
            <td class="price">'.seePrice($data['price'], FALSE).'</td>
            <td>'.$data['device'].'</td>
            <td>'.date('j M Y - H:i', $data['time']).'</td>
        </tr>';

            }

            echo '
        <tr>
            <td>Umsatzerl&ouml;s</td>
            <td class="price">'.seePrice($price, FALSE).'</td>
            <td></td>
            <td></td>
        </tr>';

            ?>
            </tbody>
        </table>
    </div>

<?php

include 'template/foot.php';
