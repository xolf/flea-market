<?php

include 'core/init.php';

if(isset($_POST['seen']))
{

    if($_POST['seller'] != '' AND $_POST['price'] != '')
    {

        if(!isset($_SESSION['flea-market']['objects'])) $_SESSION['flea-market']['objects'] = array();

        $price = strToFloat($_POST['price']);

        array_push($_SESSION['flea-market']['objects'], array('seller' => $_POST['seller'], 'price' => $price));

        $json = json_encode($_SESSION['flea-market']['objects']);

        $query = "UPDATE `devices` SET `objects` = '".$json."' WHERE `name` = '".$_COOKIE['device-name']."'";

        $mysql->query($query);

    }
    else
    {

        $message = "<h2 class='center seller'>Es m&uuml;ssen alle Felder ausgef&uuml;llt werden!</h2>";

    }


}

include 'template/head.php';

?>

    <div class="sell-item">
        <form method="post">
            <?php echo $message ?>
            <h3>Verk&auml;ufer Nummer</h3>
            <input type="hidden" name="seen" value="3dr">
            <div class="input-group">
                <span class="input-group-addon" id="addon" name="seller">V</span>
                <input id="seller" type="number" class="form-control input-lg" aria-describedby="addon" name="seller" autofocus required>
            </div>
            <div class="numpad" id="numpad-seller">
                <?php include 'template/numpad.php' ?>
            </div>
            <h3>Preis</h3>
            <div class="input-group">
                <input id="price" type="number" pattern="[0-9]+([\.|,][0-9]+)?" step="0.01" class="form-control input-lg" aria-describedby="addon" name="price" required>
                <span class="input-group-addon" id="addon" name="price">&euro;</span>
            </div>
            <div class="numpad" id="numpad-price">
                <?php include 'template/numpad.php' ?>
            </div>
            <input type="submit" value="Hinzuf&uuml;gen" class="btn btn-primary btn-lg full-width"><br><br>
            <a href="cart.php" class="btn btn-lg btn-default right goRight"><b>>></b> Weiter</a>
        </form>

    </div>

<?php

include 'template/foot.php';
