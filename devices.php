<?php

include 'core/init.php';

include 'template/head.php';

?>

    <div class="container">
        <h1>Kassenliste</h1>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Kassenname</th>
                <th>Registriert</th>
                <th>Kassenstand</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <?php

            $result = $mysql->query("SELECT `device`, sum(`price`) as `price` FROM `sold_objects` GROUP BY `device`");

            while($data = mysqli_fetch_assoc($result))
            {

                $amount[$data['device']] = $data['price'];

            }

            $result = $mysql->query("SELECT * FROM `devices` ORDER BY `name`");

            $i = 1;

            $total = 0;

            while($data = mysqli_fetch_assoc($result))
            {

                $status = 'Aktiv';

                if($data['objects'] == 'null') $status = 'Inaktiv';

                echo '
        <tr>
            <td>'.$i.'</td>
            <td>'.$data['name'].'</td>
            <td>'.date('j M Y - H:i', $data['signup']).'</td>
            <td>'.seePrice(strToFloat($amount[$data['name']]) + $data['amount'], FALSE).'</td>
            <td>'.$status.'</td>
        </tr>';

                $i++;

                $total = $total + $amount[$data['name']];

            }

            echo '
        <tr>
            <td></td>
            <td>Alle Kassen zusammen</td>
            <td></td>
            <td>'.seePrice($total, FALSE).'</td>
            <td></td>
        </tr>';


            ?>
            </tbody>
        </table>
    </div>

<?php

include 'template/foot.php';
