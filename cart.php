<?php

include 'core/init.php';

include 'template/head.php';

?>

    <div class="cart">
        <a href="add-object.php" class="btn btn-primary btn-lg goLeft"><b>+</b> Neuer Artikel</a>
        <a href="checkout.php" class="btn btn-default btn-lg right"><b>>></b> Bezahlung</a>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Verk&auml;ufer Nummer</th>
                <th>Preis</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php

            $i = 1;

            $endData['price'] = 0;

            foreach($_SESSION['flea-market']['objects'] as $object)
            {

                $endData['price'] += $object['price'];

                echo '
            <tr>
                <td>'.$i.'</td>
                <td class="seller">V'.$object['seller'].'</td>
                <td class="price">'.seePrice($object['price'], FALSE).'</td>
                <td><a class="btn btn-danger" href="drop-object.php?i='.$i.'">X</a></td>
            </tr>';

                $i++;
            }

            $_SESSION['flea-market']['endData'] = $endData;

            echo '
            <tr>
                <td>#</td>
                <td>Endsumme</td>
                <td class="price">'.seePrice($endData['price'], FALSE).'</td>
                <td></td>
            </tr>';

            ?>
            </tbody>
        </table>
        <a href="checkout.php" class="btn btn-default btn-lg right goRight"><b>>></b> Bezahlung</a>
        <a href="add-object.php" class="btn btn-primary btn-lg"><b>+</b> Neuer Artikel</a>
    </div>

<?php

include 'template/foot.php';
