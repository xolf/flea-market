<?php

include 'core/init.php';

$message = '';

$given = '';

if(isset($_POST['seen']))
{

    $given = strToFloat($_POST['given']);

    if($given >= $_SESSION['flea-market']['endData']['price'])
    {

        $message = 'R&uuml;ckgeld: ' . seePrice($given - $_SESSION['flea-market']['endData']['price'], FALSE);

    }
    else
    {

        $message = 'Es wurde nicht gen&uuml;gend Geld gegeben';

    }

}

include 'template/head.php';

?>

    <div class="checkout">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h1>Zu zahlen: <?php seePrice($_SESSION['flea-market']['endData']['price']) ?></h1>
            <h3>Gegeben in &euro;: </h3>
            <form method="post">
                <input type="hidden" name="seen" value="rtz">
                <div class="input-group">
                    <span class="input-group-addon" id="addon">&euro;</span>
                    <input id="price" type="number" pattern="[0-9]+([\.|,][0-9]+)?" step="0.01" class="form-control input-lg" aria-describedby="addon" name="given" required autofocus">
                </div>
            </form>
            <h1><?php echo $message ?></h1>
            <a href="cart.php" class="btn btn-lg btn-primary left goLeft"><b><<</b> Zur&uuml;ck</a>
            <a href="new-customer.php" class="btn btn-lg btn-default right goRight"><b>>></b> N&auml;chster Kunde</a>
        </div>
        <div class="col-md-3"></div>
    </div>

<?php

include 'template/foot.php';
